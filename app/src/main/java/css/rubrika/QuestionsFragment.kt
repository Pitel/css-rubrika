package css.rubrika

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.content.edit
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.lifecycle.observe
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.questions.*

class QuestionsFragment() : Fragment(R.layout.questions) {
    constructor(phase: Long) : this() {
        arguments = bundleOf(PHASE_ARGS_KEY to phase)
    }

    private val viewModel: RemoteConfigViewModel by activityViewModels()
    private val prefs by lazy { PreferenceManager.getDefaultSharedPreferences(context) }
    private val phase by lazy { requireArguments().getLong(PHASE_ARGS_KEY, 1) }

    init {
        setHasOptionsMenu(true)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as MainActivity).supportActionBar?.setTitle(
            when (phase) {
                3L -> R.string.title_end
                else -> R.string.title
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler.setHasFixedSize(true)
        val adapter = QuestionsAdapter(phase)
        recycler.adapter = adapter
        viewModel.questions.observe(viewLifecycleOwner) { adapter.submitList(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        menu.findItem(R.id.save).isVisible =
            phase == 3L && !prefs.getBoolean(MainActivity.FINISHED_PREFS_KEY, false)
        menu.findItem(R.id.revert).isVisible = phase == 3L && BuildConfig.DEBUG
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.save -> {
            prefs.edit { putBoolean(MainActivity.FINISHED_PREFS_KEY, true) }
            recycler.adapter =
                QuestionsAdapter(phase).apply { submitList((recycler.adapter as QuestionsAdapter).currentList) }
            item.isVisible = false
            true
        }
        R.id.revert -> {
            prefs.edit { remove(MainActivity.FINISHED_PREFS_KEY) }
            parentFragmentManager.commit { replace(android.R.id.content, QuestionsFragment(phase)) }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private companion object {
        private const val PHASE_ARGS_KEY = "phase"
    }
}