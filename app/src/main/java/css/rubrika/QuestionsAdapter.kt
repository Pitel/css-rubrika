package css.rubrika

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.slider.Slider
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.question.*

class QuestionsAdapter(private val phase: Long) :
    ListAdapter<Question, QuestionsAdapter.QuestionViewHolder>(DIFF) {
    private lateinit var prefs: SharedPreferences
    private val finished by lazy { prefs.getBoolean(MainActivity.FINISHED_PREFS_KEY, false) }

    private val allDone: Boolean
        get() = prefs.all.keys.filter { it.startsWith("$ANSWER_PREFS_PREFIX$phase") }.size == currentList.size + 1 && prefs.getString(
            QUESTION_PREFS_KEY,
            null
        )?.isNotBlank() == true

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = QuestionViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.question, parent, false)
    )

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        when (getItemViewType(position)) {
            QUESTION_NORMAL -> {
                holder.title.isVisible = true
                holder.description.isVisible = true
                holder.custom.isGone = true
                holder.title.text = getItem(position).title
                holder.description.text = getItem(position).description
            }
            QUESTION_CUSTOM -> {
                holder.title.isGone = phase == 1L
                holder.title.text = prefs.getString(
                    QUESTION_PREFS_KEY,
                    holder.title.context.getString(R.string.custom_question)
                )
                holder.description.isGone = true
                with(holder.custom) {
                    isVisible = !finished && phase == 1L
                    editText?.run {
                        setText(prefs.getString(QUESTION_PREFS_KEY, null))
                        doOnTextChanged { text, _, _, _ ->
                            prefs.edit {
                                putString(QUESTION_PREFS_KEY, text.toString())
                            }
                        }
                    }
                }
            }
        }
        val key = when (phase) {
            2L -> "${ANSWER_PREFS_PREFIX}1$position"
            else -> "$ANSWER_PREFS_PREFIX$phase$position"
        }
        with(holder.slider) {
            clearOnSliderTouchListeners()
            isEnabled = !finished && phase != 2L
            value = if (finished) {
                prefs.getInt("${ANSWER_PREFS_PREFIX}3$position", 1).toFloat()
            } else {
                prefs.getInt(key, 1).toFloat()
            }
            addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
                override fun onStartTrackingTouch(slider: Slider) {}

                override fun onStopTrackingTouch(slider: Slider) {
                    prefs.edit {
                        putInt(key, value.toInt())
                    }
                    if (allDone) {
                        Toast.makeText(context, R.string.all_done, Toast.LENGTH_SHORT).show()
                    }
                }

            })
        }
        with(holder.slider2) {
            isEnabled = false
            isVisible = finished
            value = prefs.getInt("${ANSWER_PREFS_PREFIX}1$position", 1).toFloat()
        }
    }

    override fun getItemId(position: Int) = try {
        getItem(position).title.hashCode()
    } catch (ioobe: IndexOutOfBoundsException) {
        if (position == super.getItemCount()) {
            0
        } else {
            throw ioobe
        }
    }.toLong()

    override fun getItemCount() = if (super.getItemCount() > 0) {
        super.getItemCount() + 1
    } else {
        super.getItemCount()
    }

    override fun getItemViewType(position: Int) = if (position == super.getItemCount()) {
        QUESTION_CUSTOM
    } else {
        QUESTION_NORMAL
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        prefs = PreferenceManager.getDefaultSharedPreferences(recyclerView.context)
    }

    inner class QuestionViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer

    private companion object {
        private val DIFF = object : DiffUtil.ItemCallback<Question>() {
            override fun areItemsTheSame(oldItem: Question, newItem: Question) =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: Question, newItem: Question) =
                oldItem == newItem
        }

        private const val QUESTION_NORMAL = 0
        private const val QUESTION_CUSTOM = 1
        private const val QUESTION_PREFS_KEY = "q"
        private const val ANSWER_PREFS_PREFIX = 'a'
    }
}