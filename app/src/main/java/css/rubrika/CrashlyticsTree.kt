package css.rubrika

import android.util.Log.*
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

class CrashlyticsTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        FirebaseCrashlytics.getInstance().let { crashlytics ->
            crashlytics.log("${priorityIntToChar(priority)}/$tag: $message")
            if (priority >= WARN && t != null) {
                crashlytics.recordException(t)
            }
        }
    }

    private fun priorityIntToChar(priority: Int) = when (priority) {
        VERBOSE -> 'V'
        DEBUG -> 'D'
        INFO -> 'I'
        WARN -> 'W'
        ERROR -> 'E'
        ASSERT -> 'A'
        else -> priority.toChar()
    }
}