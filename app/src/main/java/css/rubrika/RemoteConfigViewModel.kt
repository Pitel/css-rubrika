package css.rubrika

import android.text.Html
import android.util.JsonReader
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import timber.log.Timber

class RemoteConfigViewModel : ViewModel() {
    private val _phase = MutableLiveData<Long>()
    val phase: LiveData<Long> = _phase

    private val _quetions = MutableLiveData<List<Question>>()
    val questions: LiveData<List<Question>> = _quetions

    init {
        Firebase.remoteConfig.fetchAndActivate().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                _phase.value = Firebase.remoteConfig["phase"].asLong()
                _quetions.value = parseQuestions(Firebase.remoteConfig["questions"].asString())
            } else {
                Timber.e(task.exception)
            }
        }
    }

    private fun parseQuestions(json: String): List<Question> {
        val questions = mutableListOf<Question>()
        JsonReader(json.reader()).use { jsonReader ->
            jsonReader.beginArray()
            while (jsonReader.hasNext()) {
                jsonReader.beginObject()
                questions += Question(jsonReader.nextName(), Html.fromHtml(jsonReader.nextString()))
                jsonReader.endObject()
            }
            jsonReader.endArray()
        }
        return questions
    }
}