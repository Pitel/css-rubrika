package css.rubrika

import android.text.Spanned

data class Question(
    val title: String,
    val description: Spanned
)