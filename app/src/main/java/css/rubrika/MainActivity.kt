package css.rubrika

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.observe
import androidx.preference.PreferenceManager

class MainActivity : AppCompatActivity() {
    private val viewModel: RemoteConfigViewModel by viewModels()
    private val prefs by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.phase.observe(this) { phase ->
                supportFragmentManager.commit {
                    replace(
                        android.R.id.content,
                        QuestionsFragment(phase)
                    )
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu) = if (BuildConfig.DEBUG) {
        menuInflater.inflate(R.menu.phase, menu)
        true
    } else {
        super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        if (item.itemId in intArrayOf(R.id.phase1, R.id.phase2, R.id.phase3)) {
            supportFragmentManager.commit {
                replace(
                    android.R.id.content, QuestionsFragment(
                        when (item.itemId) {
                            R.id.phase1 -> 1
                            R.id.phase2 -> 2
                            R.id.phase3 -> 3
                            else -> 1
                        }
                    )
                )
            }
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    companion object {
        const val FINISHED_PREFS_KEY = "finished"
    }
}